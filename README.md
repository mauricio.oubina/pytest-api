# uala-arg-template-api-capacitacion
Capacitacion Uala
## Getting Started

Las siguientes instrucciones premiten obtener una copia del proyecto, setear el entorno y poder correrlo localmente para desarollo de nuevas funcionalidades o testing.

### Prerequisitos

> #### Python 
>
> - Ingresar a la sección Descargas de [Python](https://www.python.org/downloads/).
> - Descargar la última versión o >= to 3.8.0.
> - Instalar Python y setear las variables de entorno.
> - Verificar que se haya instalado correctamenete con *python --version* desde cualquier consola/terminal (PowerShell, CMD, bash).
> ```
> PS C:\Users\you_user> python --version
> Python 3.8.0
> ```

> #### Entorno virtual de Python
>
> Los entornos virtuales de Python son útiles para evitar conflictos entre distintos proyectos que pueden utilizar distintas versiones de librerías.
> - Ubicado desde una consola en la raíz del proyecto, ejecutar el siguiente comando:
> ```
> python -m venv .venv
> ```
> - Al finalizar, se debería haber creado una carpeta con nombre ".venv" la raíz del proyecto.
> - Para acitvar el entorno virtual, ejecutar el siguiente comando:
> -- En Linux bash/zsh -> ``` $ source .venv/bin/activate ```
> -- En Windows cmd.exe -> ``` .\.venv\Scripts\activate.bat ```
> -- En Windows PowerShell -> ``` .\.venv\Scripts\Activate.ps1 ```
> - Para indagar más sobre el tema, ingresar a la siguiente url [venv](https://docs.python.org/3/library/venv.html).

> #### Java
>
> Es necesario instalar Java para poder generar y levantar el reporte de Allure.
> - Descargar la versión correspondietnte al SO [Oracle](https://www.oracle.com/ar/java/technologies/javase/javase-jdk8-downloads.html).
> - Instalar Java. 

> #### Allure 
>
> Se necesita instalar Allure en el sistema.
> - Ingresar a [allure release](https://github.com/allure-framework/allure2/releases/).
> - Descargar la versión zip: **2.13.3**.
> - Descomprimir en el directorio que prefieras. Por ejemplo, crear una carpeta de nombre *allure* en *C:\\* y descomprimir aquí.
> ```
> C:\allure
> ```
> - Agregar la carpeta *bin* de allure a las Variables de Entorno *PATH*.
> - Para obtener ayuda sobre el paso anterior, ingresar a [how to set a environment variable on Windows 10](https://superuser.com/questions/949560/how-do-i-set-system-environment-variables-in-windows-10).
> - Verificar la instalación de Allure ejecutando el comando *allure --version* desde cualquier consola/terminal.
> ```
> PS C:\Users\you_user> allure --version
> 2.13.3
> ```

> #### VPN Ualá
>
> - Para poder ejecutar los test localmente es necesario disponer y estar conectado de una VPN de Ualá.
> - En caso que no se posea, comunicarse con el sector correspondiente.

### Instalación

>
> #### Python Libs
> - Es neceasrio instalar en el proyecto los módulos/librerías que se usan como dependencias desde el archivo *requirements.txt*.
> ```
> PS C:\Users\you_user\you_workspace\uala-col-integrator-qa> pip install -r requirements.txt
> ```
> - Finalizada la instalación, se puede verificar la instalación de los módulos con el comando *pip freeze* y se debe observar lo siguente:
> ```
> PS C:\Users\you_user\you_workspace\uala-col-integrator-qa> pip freeze
> ...
> allure-pytest-bdd==2.8.22
> allure-python-commons==2.8.13
> pytest==5.4.1
> selenium==3.141.0
> ...
>```