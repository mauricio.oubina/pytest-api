from src.Shorten import Shorten

class shorten_steps:

    def postShorten(self):
        path = '/shorten'
        token = '5bc1a34c40mshe27d32ebba28f93p1f3a18jsn222d7956a53b'
        body = 'url=https://google.com/'
        return Shorten.postShorten(self, path, token, body)

    def postEmptyBodyUrl(self):
        path = '/shorten'
        token = '5bc1a34c40mshe27d32ebba28f93p1f3a18jsn222d7956a53b'
        body = 'url='
        return Shorten.postShorten(self, path, token, body)

    def postInvalidToken(self):
        path = '/shorten'
        token = ''
        body = 'url=https://google.com/'
        return Shorten.postShorten(self, path, token, body)

    def postInvalidPathUrl(self):
        path = '/shorte'
        token = '5bc1a34c40mshe27d32ebba28f93p1f3a18jsn222d7956a53b'
        body = 'url=https://google.com/'
        return Shorten.postShorten(self, path, token, body)

    def postEmptyBody(self):
        path = '/shorten'
        token = '5bc1a34c40mshe27d32ebba28f93p1f3a18jsn222d7956a53b'
        body = ''
        return Shorten.postShorten(self, path, token, body)
