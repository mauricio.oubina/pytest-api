import pytest
import allure
from steps.Shorten_Steps import shorten_steps

class TestShorten:

    pytestmark = [
        allure.suite("Shorten")
    ]

    @pytest.mark.shorten
    def testShortenOk(self):
        response = shorten_steps.postShorten(self)
        print(response.json())

        assert (
                response.status_code == 200
        ), f"Response was not successful, status [{response.status_code}], body [{response.json()}]"

        assert response.json()["result_url"] == 'https://goolnk.com/AqJYaW'
        return


    @pytest.mark.shorten
    def testEmptyUrl(self):
        response = shorten_steps.postEmptyBodyUrl(self)
        print(response.json())

        assert (
                response.status_code == 400
        ), f"Response was not successful, status [{response.status_code}], body [{response.json()}]"

        assert response.json()["error"] == 'API Error: After sanitization URL is empty'
        return

    @pytest.mark.shorten
    def testInvalidToken(self):
        response = shorten_steps.postInvalidToken(self)
        print(response.json())

        assert (
                response.status_code == 401
        ), f"Response was not successful, status [{response.status_code}], body [{response.json()}]"

        assert response.json()["message"] == 'Invalid API key. Go to https://docs.rapidapi.com/docs/keys for more info.'
        return

    @pytest.mark.shorten
    def testInvalidPathForUrl(self):
        response = shorten_steps.postInvalidPathUrl(self)
        print(response.json())

        assert (
                response.status_code == 404
        ), f"Response was not successful, status [{response.status_code}], body [{response.json()}]"

        assert response.json()["message"] == 'Endpoint/shorte does not exist'
        return

    @pytest.mark.shorten
    def testEmptyBody(self):
        response = shorten_steps.postEmptyBody(self)
        print(response.json())

        assert (
                response.status_code == 400
        ), f"Response was not successful, status [{response.status_code}], body [{response.json()}]"

        assert response.json()["error"] == 'API Error: URL is empty'
        return