import json
from commons_qa.api.api_client import ApiClient

class Shorten:

    def postShorten(self, path, token, body):
        api_client = ApiClient('https://url-shortener-service.p.rapidapi.com')
        headers = {
            'content-type': "application/x-www-form-urlencoded",
            'X-RapidAPI-Key': token,
            'X-RapidAPI-Host': "url-shortener-service.p.rapidapi.com"
        }
        payload = body
        url_encoded = True
        return api_client.post(path=path,payload=payload, params={}, headers=headers, url_encoded=url_encoded)
