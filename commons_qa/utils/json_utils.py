"""
Archivo .py con la funciones que permiten manejar archivos Json
"""
import json
from decimal import Decimal


def to_json(obj):
    """
    Método que transforma un diccionario en una string en formato JSON

    :param obj: dict, objeto a transformar
    :return type: String
    :return: Objeto transformado a json (incluyendo salto de lineas)

    Ejemplo::

       example_dict = {"age": 20, "name": "John", "lastName": "Doe"}
       json_result = to_json(example_dict)
       print(json_result)
       >>  {
               "age": 20,
               "lastName": "Doe",
               "name": "John"
           }
    """
    return json.dumps(obj, default=lambda o: __default_converter(o),
                      sort_keys=True, indent=4)


def read_object_from_file(file):
    """
    Método que genera un diccionario a partir de un archivo .json

    :param file: file, archivo .json a generar
    :return type: dict
    :return: Objeto generado

    Ejemplo::

       json_from_file = read_object_from_file(filepath)
       key_a = json_from_file['keyA']
       key_b = json_from_file.get('keyB')
    """
    with open(file) as f:
        data = json.load(f)
    return data


def __default_converter(obj):
    if isinstance(obj, Decimal):
        return str(obj)
    return obj.__dict__
