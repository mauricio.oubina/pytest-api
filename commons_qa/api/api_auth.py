"""
Archivo py con la clase encargada de agregar la autorizacion de la API en el header
"""
from requests.auth import AuthBase


class AuthorizationHeaderAuth(AuthBase):
    """
    Clase encargada de agregar la autorizacion de la API en el header Authorization
    """
    def __init__(self, token):
        self._token = token

    def __call__(self, r):
        r.headers['Authorization'] = self._token
        return r


class SecretKeyHeaderAuth(AuthBase):
    """
    Clase encargada de agregar la autorizacion de la API en el header secretkey
    """
    def __init__(self, secret_key):
        self._secret_key = secret_key

    def __call__(self, r):
        r.headers['secretkey'] = self._secret_key
        return r
