import logging

from pydantic import BaseModel

from commons_qa import logger
from commons_qa.api.api_client import ApiClient
from commons_qa.api.salesforce.salesforce_auth import SalesforceAuth


class SalesforceApi(ApiClient):

    """
    Clase que proporciona la interaccion con los distintos endpoints de Salesforce
    """

    def __init__(self):
        self.token = SalesforceAuth().salesforce_token
        self.log = logger.get_logger(__name__)
        super().__init__(api_url=self.token.instance_url, token=f'Bearer {self.token.access_token}')

    def get_opportunity(self, oportunity_id):
        """
        Obtiene la oportunidad en Salesforce
        :param oportunity_id: Id de la oportunidad que se genera en AWS
        :return:
        """
        query = f'Select+Id+From+Opportunity+WHERE+AWSLoanId__c=\'{oportunity_id}\''
        return self.get(path=f'/services/data/v52.0/query?q={query}').json()

    def get_opportunity_details(self, oportunity_id):
        """
        Obtiene todos los detalles de la oportunidad en Salesforce
        :param oportunity_id: Id de la oportunidad que se genera en AWS
        :return:
        """
        try:
            path = self.get_opportunity(oportunity_id)['records'][0]['attributes']['url']
        except Exception as e:
            self.log.error('The opportunity does not exist in Salesforce')
            self.log.error(e.__traceback__)
            return None

        response = self.get(path=path)
        return OportunityDetails(**response.json())


class OportunityDetails(BaseModel):
    Id: str
    Name: str
    StageName: str
    AWSLoanId__c: str
    AmountSimulator__c: float