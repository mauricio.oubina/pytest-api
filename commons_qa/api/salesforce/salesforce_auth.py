import logging

from commons_qa.api.api_client import ApiClient
from typing import List, Optional
from pydantic import BaseModel
from commons_qa.api.salesforce.salesforce_credentials import SalesforceCredentials


class SalesforceAuth(ApiClient):

    """
    Clase encargada de manejar la autenticación contra Salesforce
    """

    def __init__(self):
        """
        Constructor
        """

        super().__init__(api_url='https://test.salesforce.com/services/oauth2/token')
        self.credentials = SalesforceCredentials().get_credentials()
        self.salesforce_token = self.__get_user_authorization_token()

    def __get_user_authorization_token(self):
        """
        Obtiene el token del usuario invocando la api de Salesforce mediante OAuth2
        :param username: Usuario del que se quiere obtener el token
        :param password: Password del usuario del que se quiere obtener el token
        :return: Token del usuario que se necesita como Bearer Authorization
        """
        params = {'grant_type': 'password',
                  'client_id': self.credentials.client_id,
                  'client_secret': self.credentials.client_secret,
                  'username': self.credentials.username,
                  'password': f'{self.credentials.password}{self.credentials.user_token}'}

        response = self.post(payload={}, params=params)

        if not response.ok:
            logging.error(f"Response status code: {response.status_code}")
            logging.error(f"Response error message: {response.json()}")
            raise SalesforceTokenError("There was an error trying to fetch Salesforce user authentication token.")

        return SalesforceToken(**response.json())


class SalesforceTokenError(Exception):
    pass


class SalesforceToken(BaseModel):
    access_token: str
    instance_url: str
    id: Optional[str] = None
    token_type: Optional[str] = None
    issued_at: Optional[str] = None
    signature: Optional[str] = None
