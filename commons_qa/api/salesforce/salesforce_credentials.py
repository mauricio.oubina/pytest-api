import os


class Credentials:

    def __init__(self, username, password, user_token, client_id, client_secret):
        self.username = username
        self.password = password
        self.user_token = user_token
        self.client_id = client_id
        self.client_secret = client_secret


class SalesforceCredentials:

    def __init__(self):

        self.credentials = {
            'dev': Credentials(username='prestamos.mex@uala.com.ar.dev',
                               password='Uala.2021!',
                               user_token='pGYurNd3zJaK7FpgosH8pUd4',
                               client_id='3MVG9lJB4lV8F4SjQdgopDVNpU9lx4NH0Eu8uwx7jPoWGaXMduMJEibnkJ8gA.dM0XRL86Nld0YvXNGUZoU7d',
                               client_secret='836CDAC216490DB3570A83D1D990DC3E1D5D5C36A769C01CDC654D82AB366C0A')
            ,
            'test': Credentials(username='prestamos.mex@uala.com.ar.ualaqa',
                                password='Uala.2021!',
                                user_token='y36oGsAc7MBoe3KGbgF71LWb',
                                client_id='3MVG9M6Iz6p_Vt2xTZYciaJsvxw5z3qKGpoxke9w0rdaFE3vIpY03rwUZSiJTYx8fDSmcfuTv2Na_a20JABwd',
                                client_secret='FD83F0DFA9949762A661E862ED56ABD962948C5B7504F9DB68B3182CD44C2EED')
            ,
            'stage': Credentials(username='prestamos.mex@uala.com.ar.uat',
                                 password='Uala.2021!',
                                 user_token='cze6LAERFF0k2JiCHHTVmCmk5',
                                 client_id='3MVG9lJB4lV8F4SgtwK0Ufm0s8vzlLToMjPPTr2AMmINu78rapcfB6Vvp27R9FTgZEBxkXxh_xUBwR4hJp5kC',
                                 client_secret='3019C93C0118DF1B0F690D69823EB534437212E0BC01C38CDDB5081852E9CDFE')
        }

    def get_credentials(self):
        env = os.environ['env']
        return self.credentials[env]



