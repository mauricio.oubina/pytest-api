"""
Archivo py con la clase encargada de manejar peticiones http (GET-POST-PUT-DELETE)
"""
import requests
from commons_qa import logger
from commons_qa.api.api_auth import AuthorizationHeaderAuth, SecretKeyHeaderAuth
from commons_qa.utils.json_utils import to_json


class ApiClient:
    """
    Clase encargada de manejar peticiones http (GET-POST-PUT-DELETE)
    """

    def __init__(self, api_url, token=None, user=None, password=None, auth=None, secretkey=None):
        """
        Instanciacion de ApiClient


        :param api_url: La url base del servicio al que se le quiere peticionar
        :param token: (Opcional) Token de acceso proporcionado por Cognito para un usuario
        :param user: (Opcional) Usuario en caso de que el servicio requiera Basci Auth
        :param password:(Opcional) Password en caso de que el servicio requiera Basci Auth
        """
        self._log = logger.get_logger(__name__)
        self._api_url = api_url
        if auth:
            self.__auth = auth
        elif user and password:
            self.__auth = (user, password)
        elif token:
            self.__auth = AuthorizationHeaderAuth(token)
        elif secretkey:
            self.__auth = SecretKeyHeaderAuth(secretkey)
        else:
            self.__auth = None

    def get(self, path='', params=None, headers={}, verify=True):
        """
        Realizar una peticion GET.


        :param path: (Opcional) Path asociado al metodo a invocar
        :param params: (Opcional) Query Params del metodo
        :param headers: (Opcional) Header a enviar al servicio.
                    Default: {'Accept': 'application/json', 'Content-Type': 'application/json'}
        :param verify: (Opcional) Boolean
        :return type: Response
        :return: La respuesta del servicio
        """
        request_url = self._api_url + path
        self._log.info("GET " + request_url)
        response = requests.get(request_url, params, headers=self._get_headers(headers), auth=self.__auth, verify=verify)

        self._log.info("Response: " + str(response))
        return response

    def post(self, payload, path='', headers={}, url_encoded=False, verify=True, params=None):
        """
        Realizar una peticion POST.

        El método POST se utiliza para enviar una entidad a un recurso en específico, causando a menudo
        un cambio en el estado o efectos secundarios en el servidor.


        :param payload: Body a enviar al servicio en el metodo POST
        :param path: (Opcional) Path asociado al metodo a invocar
        :param headers: (Opcional) Header a enviar al servicio.
                    Default: {'Accept': 'application/json', 'Content-Type': 'application/json'}
        :param url_encoded: (Opcional) Boolean
        :param verify: (Opcional) Boolean
        :param params: (Opcional) Query Params del metodo
        :return type: Response
        :return: La respuesta del servicio
        """
        request_url = self._api_url + path
        self._log.info("POST " + request_url)
        input = {
            'url': request_url,
            'timeout': None,
            'auth': self.__auth,
            'verify': verify,
            'params': params
        }

        if url_encoded:
            input['data'] = payload
            input['headers'] = headers
            self._log.info("Body: " + str(payload))
        else:
            data = to_json(payload)
            input['headers'] = self._get_headers(headers)
            input['data'] = data
            self._log.info("Body: " + data)

        response = requests.post(**input)
        self._log.info("Response: " + str(response))
        return response

    def delete(self, path='', headers={}):
        """
        Realizar una peticion DELETE.


        :param path: (Opcional) Path asociado al metodo a invocar
        :param headers: (Opcional) Header a enviar al servicio.
                        Default: {'Accept': 'application/json', 'Content-Type': 'application/json'}
        :return type: Response
        :return: La respuesta del servicio
        """
        request_url = self._api_url + path
        self._log.info("DELETE " + request_url)
        response = requests.delete(request_url, headers=self._get_headers(headers), auth=self.__auth)

        self._log.info("Response: " + str(response))
        return response

    def put(self, payload, path='', headers={}, verify=True):
        """
        Realizar una peticion PUT


        :param payload: Body a enviar al servicio en el metodo PUT
        :param path: (Opcional) Path asociado al metodo a invocar
        :param headers: (Opcional) Header a enviar al servicio.
                    Default: {'Accept': 'application/json', 'Content-Type': 'application/json'}
        :param verify: (Opcional) Boolean
        :return type: Response
        :return: La respuesta del servicio
        """
        request_url = self._api_url + path
        self._log.info("PUT " + request_url)
        data = to_json(payload)
        self._log.info("Body: " + data)

        response = requests.put(request_url, data=data, headers=self._get_headers(headers), timeout=None, auth=self.__auth,
                                verify=verify)

        self._log.info("Response: " + str(response))
        return response

    def patch(self, payload, path='', headers={}, verify=True):
        """
        Realizar una peticion PATCH


        :param payload: Body a enviar al servicio en el metodo PATCH
        :param path: (Opcional) Path asociado al metodo a invocar
        :param headers: (Opcional) Header a enviar al servicio.
                    Default: {'Accept': 'application/json', 'Content-Type': 'application/json'}
        :param verify: (Opcional) Boolean
        :return type: Response
        :return: La respuesta del servicio
        """
        request_url = self._api_url + path
        self._log.info("PATCH " + request_url)
        data = to_json(payload)
        self._log.info("Body: " + data)

        response = requests.patch(request_url, data=data, headers=self._get_headers(headers), timeout=None,
                                  auth=self.__auth,
                                  verify=verify)

        self._log.info("Response: " + str(response))

        return response

    def _get_headers(self, headers={}):
        header = {'Accept': 'application/json',
                  'Content-Type': 'application/json'}
        for key, value in headers.items():
            header[key] = value
        return header

    def _format_error(self, response):
        return 'Response Error. Status code: [' + str(
            response.status_code) + '] Reason: [' + response.reason + ']'

    def _set_token(self, token):
        self.__auth = AuthorizationHeaderAuth(token)
