"""
Archivo py con la clase encargada de manejar la conexion contra Auth0
"""
import logging
import jwt

from commons_qa.api.api_client import ApiClient
from commons_qa.configs.auth0_configs import Auth0Configs


class Auth0API(ApiClient):
    """
    Clase encargada de manejar la conexion contra Auth0
    """

    def __init__(self):
        self.config = Auth0Configs().get_credentials()
        super().__init__(api_url=self.config['base_url'])

    def get_user_authorization_token(self, username, password):
        """
        Obtiene el token del usuario invocando la api de Auth0
        :param username: Usuario del que se quiere obtener el token
        :param password: Password del usuario del que se quiere obtener el token
        :return type: String
        :return: Token del usuario que se necesita como Bearer Authorization
        """

        payload = {"client_id": self.config['client_id'],
                   "client_secret": self.config['client_secret'],
                   "grant_type": "password",
                   "audience": self.config['audience'],
                   "device":"00000",
                   "username": username,
                   "password": password}

        user_token = self.post(payload=payload, path='oauth/token')

        if not user_token.ok:
            logging.error(f"Response error message: {user_token.json()}")
            raise ConnectionError(f"Response endpoint with status code {user_token.status_code},"
                                  f"Response content {user_token.content}")

        return user_token.json()["access_token"]

    def get_access_token_auth0_decoded(self, username, password):
        """
        Decodeamos con el jwt (libreria externa) el token de auth0 para usar con los endpoint de las cuentas en ARG.
        """
        access_token_auth0 = self.get_user_authorization_token(username=username, password=password)
        decoded_token = jwt.decode(access_token_auth0.replace("Bearer ", ""), verify=False)
        token = decoded_token["http://cognito-proxy.ua.la/cognito_token"]
        logging.info("Data Response Cognito Access Token for User")
        return token