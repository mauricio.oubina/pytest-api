import os


class Auth0Configs:

    def __init__(self):
        self.configs = {
            "dev": {
                "arg": {
                    "client_id": "1FlNnxdwmdsmcBx2Fd1mILdCe7bOHTOH",
                    "client_secret": "qGI6qlwTM9WchHrMvXufPloY0b0JKL-S-pQAny94_tEawfYfa-WuC05cSYsx6WmV",
                    "audience": "https://uala-arg-app-dev",
                    "base_url": "https://uala-arg-app-dev.us.auth0.com/"
                },
                "mex": {
                    "client_id": "oKEA8rKDlG5i3LncaZeyaUpL9CQeOflV",
                    "client_secret": "n6SEN72OlBgTOTPcoQHLdMdZWKa1Ur7Z0sSLPosCbmMCZMUcNeV1QutCrzEfaazv",
                    "audience": "https://uala-mex-app-dev",
                    "base_url": "https://uala-mex-app-dev.auth0.com/"
                },
                "mex_bank": {
                    "client_id": "V9uvmfAzgHE7I3DKMP4ci1oVAMbkdnQl",
                    "client_secret": "ZxZqjRP3S_5psq9jVMbI3lh2ieoqQ6BxFcbFUu_DAPwV-fVNnRp7FYG117AVAdOB",
                    "audience": "https://uala-mex-bank-app-dev",
                    "base_url": "https://uala-mex-bank-dev.us.auth0.com/"
                },
                "col": {
                    "client_id": "dK2Wb6U2NYGcxWWLjOWEZiJfSfhBohw5",
                    "client_secret": "6FgbpqjnXUDQVGkxLxYfwnuTPnS-XqwIrhzY5LHd_2j4nZBmdjCLkENzJkUAvkXg",
                    "audience": "https://uala-col-app-dev",
                    "base_url": "https://uala-col-app-dev.us.auth0.com/"
                }
            },
            "test": {
                "mex": {
                    "client_id": "b7WIQ0G0rg6wckKaV3DVQfiBMpqtFzZp",
                    "client_secret": "AemEMdoxpBtYut7RBis6aDlTyh_56_DSfZ0oYlLUFYS7Jmx-GiRaiTrA5ZtzzxkN",
                    "audience": "https://uala-mex-app-test",
                    "base_url": "https://uala-mex-app-test.us.auth0.com/"
                },
                "col": {
                    "client_id": "RxLCzqzruqfVOzZWmgAEauRc3aQyLpdx",
                    "client_secret": "ffEEDbrsVpSQmHJ-1X8wrjhWVJnspt-ptUHIQTsZZWSvcf3VivXm5u3yImV9HIUO",
                    "audience": "https://uala-col-app-test",
                    "base_url": "https://uala-col-app-test.us.auth0.com/"
                }
            },
            "stage": {
                "arg": {
                    "client_id": "DMJf1oKKYwgb66G7Pfb1WSfzBv48Va6j",
                    "client_secret": "bK6ACRF6vDTsV7kajDMa6KotfbK--j9I4W5cN1i0VJQCDMPivubjuMuo7UW5uWQF",
                    "audience": "https://uala-arg-app-stage",
                    "base_url": "https://uala-arg-app-stage.us.auth0.com/"
                },
                "mex": {
                    "client_id": "ke56fqRzOF0MNLozx3gjGImAAnbtHlTi",
                    "client_secret": "Ko-Ad3D_ATiLwxOnfFgSWWZMFIOpwYDL1PMyvIHXdrvXitqCnRnlu9xrsbHePTSc",
                    "audience": "https://uala-mex-app-stage",
                    "base_url": "https://uala-mex-app-stage.auth0.com/"
                },
                "mex_bank": {
                    "client_id": "JfpoLln20b7zuqqM4Dk1BdCw3gGjVm8T",
                    "client_secret": "jk8DjMVq2Als7hMwD_H5Pcr6I0QTHxNRvqP6CER-jTteASjMsDEW2-qogn64qYMn",
                    "audience": "https://uala-mex-bank-app-stage",
                    "base_url": "https://uala-mex-bank-stage.us.auth0.com/"
                },
                "col": {
                    "client_id": "OJpSLyGsNoVKrx8iBD8WeznD0b1kXlFf",
                    "client_secret": "hxUnzf_iSc8a4GRQcqNSJRYRbSStmlCa-8Itac_k7b3A8xMH7qLIzCiLdOK_-XV0",
                    "audience": "https://uala-col-app-stage",
                    "base_url": "https://uala-col-app-stage.us.auth0.com/"
                }
            }
        }

    def get_credentials(self):
        return self.configs[os.environ['env']][os.environ['country']]
