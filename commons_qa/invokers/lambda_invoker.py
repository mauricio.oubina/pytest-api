"""
Archivo py con la clase encargada de conectarse con el servicio Lambdas de AWS
"""
import json
import logging

from commons_qa.configs.authenticator import Authenticator
from commons_qa.profiles.profile_loader import ProfileLoader


class LambdaInvoker:
    """
    Esta clase contiene todos los métodos necesarios para conectarse con el servicio de Lambdas de AWS
    """
    AWS_SERVICE_NAME = 'lambda'

    def __init__(self, account):
        self._environment = ProfileLoader.get_profile().environment
        self._client = Authenticator.for_account_and_service(account, self.AWS_SERVICE_NAME).client
        self.logger = logging.getLogger(__name__)

    def invoke(self, payload, function_name, invocation_type='RequestResponse', log_type='Tail', qualifier=None ):
        """
        Método para activar o invocar un lambda.

        :param payload: Input que necesita el lambda para activarse, formato JSON.
        :param function_name: Nombre de la función lambda que queremos invocar.
        :param invocation_type: Forma en la que invocaremos al lambda:

            - RequestResponse: invoca de forma sincronica.
            - Event: invoca de forma asincronica.
            - DryRun: valida que los parametros y los permisos del usuario.
        :param log_type: Tail para traer el log de la ejecucion en la respuesta.
        :param qualifier: se puede invocar una version especifica del lambda por version o alias.
        :return type: dict
        :return: Retorna el log de la Lambda, las datos contenido en la respuesta se pueden obtener a través del Payload de la misma (Ver el ejemplo)

            Sintáxis de la respuesta::

                {
                    'StatusCode': 123,
                    'FunctionError': 'string',
                    'LogResult': 'string',
                    'Payload': StreamingBody(),
                    'ExecutedVersion': 'string'
                }

        Ejemplo::

                def invoke_lambda_verify_document_number_availability_service(self, document_number):
                    self._log.info('Verifying document number availability')
                    payload = ({'documentNumber': document_number})
                    response = self.lambda_invoker.invoke(payload, function_name='VerifyDocumentNumberAvailabilityService')
                    data = response['Payload']
                    return data.read().decode()
        """
        self.logger.info("Invoking lambda function: {}".format(function_name))
        if qualifier is None:
            qualifier = self._environment

        response = self._client.invoke(
            FunctionName=function_name,
            InvocationType=invocation_type,
            Qualifier=qualifier,
            LogType=log_type,
            Payload=json.dumps(payload)
        )
        return response
