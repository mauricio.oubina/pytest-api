"""
Archivo py con la clase encargada de manejar la conexión con el servicio cognito-identity
"""
from commons_qa import logger

from commons_qa.configs.authenticator import Authenticator


class CognitoIdentityInvoker:
    """
    Clase encargada de manejar la conexión con el servicio cognito-identity

    Amazon Cognito Identities es un servicio web que ofrece credenciales temporales con alcance a dispositivos móviles y otros entornos que no son de confianza.
    Identifica un dispositivo de forma única y proporciona al usuario una identidad coherente durante la vida útil de una aplicación.
    Con las identidades de Amazon Cognito, se puede habilitar la autenticación con uno o más proveedores de identidad de terceros (Facebook, Google o Iniciar sesión con Amazon)
    o un grupo de usuarios de Amazon Cognito.
    Cognito ofrece un identificador único para cada usuario y actúa como un proveedor de tokens OpenID en el que AWS Security Token Service (STS) confía para acceder a credenciales de AWS temporales con privilegios limitados.
    """

    def __init__(self, account, identity_pool_id):
        """
        Método para crear un objeto de tipo CognitoIdentityInvoker

        :param account: Nombre de la cuenta, por ejemplo bancar, prepaid, etc
        :param identity_pool_id: Cognito identity_pool_id

        Ejemplo::

           cognito_identity_invoker = CognitoIdentityInvoker('bancar', cognito_identity_pool_id)
        """
        self._client = Authenticator.for_account_and_service(
            account, 'cognito-identity'
        ).client
        self.logger = logger.get_logger(__name__)
        self._identity_pool_id = identity_pool_id

    def get_id(self, logins):
        """
        Método que genera un id de Cognito

        :param logins: (dict) Un conjunto de pares de key-value opcionales que asignan nombres de proveedores a tokens de proveedores.

                       Los nombres de proveedores disponibles para inicios de sesión son los siguientes:

                          - Facebook: graph.facebook.com
                          - Amazon Cognito user pool: cognito-idp.<region>.amazonaws.com/<YOUR_USER_POOL_ID> , por ejemplo, cognito-idp.us-east-1.amazonaws.com/us-east-1_123456789
                          - Google: accounts.google.com
                          - Amazon: www.amazon.com
                          - Twitter: api.twitter.com
                          - Digits: www.digits.com

        :return type: dict
        :return:

            Sintáxis de la respuesta::

                        {
                            'IdentityId': 'string'
                        }

        Ejemplo::

             cognito_invoker = CognitoInvoker('bancar', cognito_client_id, cognito_user_pool_id)
             id_token = cognito_invoker.get_auth_token(username, password)

             cognito_identity_invoker = CognitoIdentityInvoker('bancar', cognito_identity_pool_id)
             key = f'cognito-idp.{region}.amazonaws.com/{user_pool_id}'
             logins = {key: id_token}

             identity_id = cognito_identity_invoker.get_id(logins)

        """
        response = self._client.get_id(IdentityPoolId=self._identity_pool_id, Logins=logins)
        return response

    def get_credentials_for_identity(self, identity_id, logins):
        """
        Este método devuelve las credenciales para el IdentityId proporcionado.

        :param identity_id: id de identidad generado con el método get_id
        :param logins: dict - Un conjunto de pares de key-value opcionales que asignan nombres de proveedores a tokens de proveedores.
                       Los nombres de proveedores disponibles para inicios de sesión son los siguientes:

                          - Facebook: graph.facebook.com
                          - Amazon Cognito user pool: cognito-idp.<region>.amazonaws.com/<YOUR_USER_POOL_ID> , por ejemplo, cognito-idp.us-east-1.amazonaws.com/us-east-1_123456789
                          - Google: accounts.google.com
                          - Amazon: www.amazon.com
                          - Twitter: api.twitter.com
                          - Digits: www.digits.com

        :return type: dict
        :return:

                    Sintáxis de la respuesta::

                        {
                            'IdentityId': 'string',
                            'Credentials': {
                                'AccessKeyId': 'string',
                                'SecretKey': 'string',
                                'SessionToken': 'string',
                                'Expiration': datetime(2015, 1, 1)
                            }
                        }

        Ejemplo::

             cognito_invoker = CognitoInvoker('bancar', cognito_client_id, cognito_user_pool_id)
             id_token = cognito_invoker.get_auth_token(username, password)

             cognito_identity_invoker = CognitoIdentityInvoker('bancar', cognito_identity_pool_id)
             key = f'cognito-idp.{region}.amazonaws.com/{user_pool_id}'

             logins = {key: id_token}

             identity_id = cognito_identity_invoker.get_id(logins)

             credentials = cognito_identity_invoker.get_credentials_for_identity(identity_id['IdentityId'], logins)

        """

        response = self._client.get_credentials_for_identity(IdentityId=identity_id, Logins=logins)
        return response
