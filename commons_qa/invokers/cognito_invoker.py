"""
Archivo py con la clase encargada de conectarse con el servicio CognitoIdentityProvider de AWS
"""
from commons_qa import logger

from commons_qa.configs.authenticator import Authenticator


class CognitoInvoker:
    """
    Esta clase contiene métodos que permiten conectarse con el servicio CognitoIdentityProvider de AWS
    """

    def __init__(self, account, client_id, user_pool_id):
        """
        Método para crear un objeto de tipo CognitoInvoker

        :param account: Nombre de la cuenta, por ejemplo bancar, prepaid, etc
        :param client_id: Cognito client_id
        :param user_pool_id: Cognito user_pool_id

        Ejemplo::

           cognito_invoker = CognitoInvoker('bancar', cognito_client_id, cognito_user_pool_id)
        """
        self._client = Authenticator.for_account_and_service(
            account, 'cognito-idp'
        ).client
        self.logger = logger.get_logger(__name__)
        self._client_id = client_id
        self._user_pool_id = user_pool_id

    def get_auth_token(self, username, password):
        """
        Este método retorna un token válido a partir de las credenciales de un usuario

            :param username: Nombre de usuario o e-mail del usuario registrado en Cognito
            :param password: Contraseña del usuario

            :return type: String
            :return: Token de autenticación válido


        Ejemplo::

            cognito_invoker = CognitoInvoker('bancar', cognito_client_id, cognito_user_pool_id)
            token = cognito_invoker.get_auth_token('username', 'user_password')
        """
        response_cognito = self._client.initiate_auth(
            ClientId=self._client_id,
            AuthFlow='USER_PASSWORD_AUTH',
            AuthParameters={
                'USERNAME': username,
                'PASSWORD': password,
            },
            ClientMetadata={
                'UserPoolId': self._user_pool_id
            }
        )
        token = response_cognito['AuthenticationResult']['IdToken']
        return token

    def user_sign_up(self, username, password):
        """
        Este método permite la registración de un nuevo usuario en Cognito, para crearlo exitosamente el username no debe existir en Cognito,
        caso contrario lanzará una excepción.

        :param username: Nombre de usuario o e-mail del usuario que se desea registrar
        :param password: Contraseña del usuario

        :return type: dict
        :return:

                Sintáxis de la respuesta::

                    {
                        'UserConfirmed': True|False,
                        'CodeDeliveryDetails': {
                                'Destination': 'string',
                                'DeliveryMedium': 'SMS'|'EMAIL',
                                'AttributeName': 'string'
                    },
                        'UserSub': 'string'
                    }


        Ejemplo::

            cognito_invoker = CognitoInvoker('bancar', cognito_client_id, cognito_user_pool_id)
            cognito_invoker.user_sign_up('username', 'user_password')
        """
        response_cognito = self._client.sign_up(
            ClientId=self._client_id,
            Username=username,
            Password=password,
            ValidationData=[
                {
                    "Name": "Username",
                    "Value": "string"
                },
                {
                    "Name": "Password",
                    "Value": "string"
                }
            ]
        )
        return response_cognito

    def admin_confirm_user_sign_up(self, email):
        """
        Este método permite confirmar un usuario sin usar el código de confirmación.

        :param email: Nombre de usuario o e-mail del usuario al que se le desea cambiar el estado a CONFIRMED
        :return type: dict


        Ejemplo::

            cognito_invoker = CognitoInvoker('bancar', cognito_client_id, cognito_user_pool_id)
            cognito_invoker.admin_confirm_user_sign_up('username')
        """
        response_cognito = self._client.admin_confirm_sign_up(
            UserPoolId=self._user_pool_id,
            Username=email,
        )
        return response_cognito

    def admin_get_user(self, email):
        """
        Este método nos devuelve la información del usuario a partir del username o el email

        :param email: Nombre de usuario o e-mail del usuario del que se desea recuperar la información
        :return type: dict
        :return:

            Sintáxis de la respuesta::

                    {
                    'Username': 'string',
                    'UserAttributes': [
                        {
                            'Name': 'string',
                            'Value': 'string'
                        },
                    ],
                    'UserCreateDate': datetime(2015, 1, 1),
                    'UserLastModifiedDate': datetime(2015, 1, 1),
                    'Enabled': True|False,
                    'UserStatus': 'UNCONFIRMED'|'CONFIRMED'|'ARCHIVED'|'COMPROMISED'|'UNKNOWN'|
                                  'RESET_REQUIRED'|'FORCE_CHANGE_PASSWORD',
                    'MFAOptions': [
                        {
                            'DeliveryMedium': 'SMS'|'EMAIL',
                            'AttributeName': 'string'
                        },
                    ],
                    'PreferredMfaSetting': 'string',
                    'UserMFASettingList': [
                        'string',
                    ]
                    }

        Ejemplo::

            cognito_invoker = CognitoInvoker('bancar', cognito_client_id, cognito_user_pool_id)
            cognito_user = cognito_invoker.admin_get_user('username')
        """
        response_cognito = self._client.admin_get_user(
            UserPoolId=self._user_pool_id,
            Username=email,
        )
        return response_cognito

    def delete_user_cognito(self, email):
        """
        Este método permite eliminar un usuario.

        :param email: Nombre de usuario o e-mail del usuario que se desea eliminar


        Ejemplo::

            cognito_invoker = CognitoInvoker('bancar', cognito_client_id, cognito_user_pool_id)
            cognito_user = cognito_invoker.admin_get_user('username')
        """
        self.logger.info("---------------- delete Cognito-----------------")
        self._client.admin_delete_user(
            UserPoolId=self._user_pool_id,
            Username=email
        )

    def admin_update_user_attributes(self, username, attributes):
        """
        Este método permite actualizar los atributos de un usuario.

        :param username: Nombre de usuario o e-mail del usuario al que se le desea actualizar los atributos
        :param attributes: Diccionario con los
        :return type: dict

        Ejemplo::

            cognito_invoker = CognitoInvoker('bancar', cognito_client_id, cognito_user_pool_id)
            attributes = [
                            {
                                'Name': 'email',
                                'Value': email
                            },
                            {
                                'Name': 'custom:id',
                                'Value': custom_id
                            },
                            {
                                'Name': 'phone_number',
                                'Value': phone_number
                            },
                        ]
            cognito_invoker.admin_update_user_attributes(username, attributes)
        """
        response_cognito = self._client.admin_update_user_attributes(
            UserPoolId=self._user_pool_id,
            Username=username,
            UserAttributes=attributes
        )
        return response_cognito
